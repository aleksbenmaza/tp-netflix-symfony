<?php

namespace AppBundle\Filter;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 23/02/2018
 * Time: 16:05
 */
class EMAutoFlushingAfterFilter implements EventSubscriberInterface {

    private $logger;

    private $entityManager;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $entityManager
    ) {
        $this->logger        = $logger;
        $this->entityManager = $entityManager;
    }

    public function run(): void {
        $this->logger->info(self::class . "::" . __METHOD__ . " called !");
        $this->entityManager->flush();
    }

    public static function getSubscribedEvents(): array {
        return [
            KernelEvents::VIEW => 'run',
        ];
    }
}