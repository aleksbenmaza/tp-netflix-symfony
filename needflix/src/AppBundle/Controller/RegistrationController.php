<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 28/02/2018
 * Time: 14:35
 */

namespace AppBundle\Controller;

use AppBundle\Form\RegistrationType;
use AppBundle\Service\UserService;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class RegistrationController
 * @package AppBundle\Controller
 * @Security(expression="is_granted('IS_ANONYMOUS')")
 * @Route("/register")
 */
class RegistrationController extends Controller {

    /**
     * @var UserService
     */
    private $user_service;

    /**
     * RegistrationController constructor.
     * @param UserService $user_service
     */
    public function __construct(UserService $user_service) {
        $this->user_service = $user_service;
    }


    /**
     * @Route(name="registration_index")
     * @Method("GET")
     * @Template("registration/index.html.twig")
     */
    public function registrationFormAction(): array
    {
        return [
            'form' => $this->createForm(RegistrationType::class)->createView()
        ];
    }

    /**
     * @param Request $request
     * @param Session $session
     * @return RedirectResponse|array
     *
     * @Route(name="registration_new")
     * @Method("POST")
     * @Template("registration/index.html.twig")
     */
    public function registrationAction(Request $request, Session $session)
    {

        $form = $this->createForm(RegistrationType::class);

        $registration = $form->handleRequest($request)->getData();

        if($form->isSubmitted() && $form->isValid()) {
            $user = $this->user_service->createUser($registration);

            $session->getFlashBag()->add(
                'success_message',
                'Vous venez de compléter l\'inscription, vous pouvez désormais vous connecter'
            );

            return new RedirectResponse("/login");
        }

        return [
            'form' => $form->createView()
        ];
    }
}