<?php
/**
 * Created by PhpStorm.
 * User: Arnaud
 * Date: 27/02/2018
 * Time: 17:37
 */

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class CategoryFixtures extends  Fixture
{
    public function load(ObjectManager $manager){
        $category1 = new Category();
        $category2 = new Category();


        $category1->setName("Action");
        $category1->setCode('action');

        $category2->setName("Scien fiction");
        $category2->setCode('scienfiction');

        $manager->persist($category1);
        $manager->persist($category2);

        $manager->flush();

        $this->addReference('categorie1', $category1);
        $this->addReference('categorie2', $category2);
    }
}