<?php
/**
 * Created by PhpStorm.
 * User: Arnaud
 * Date: 23/02/2018
 * Time: 15:28
 */

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Film;
use AppBundle\Entity\Category;
use AppBundle\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class FilmFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
       // $category = new Category();

        //$category = $manager->getRepository(Category::class)->findAll();

        $film1 = new Film($this->getReference('categorie2'));
        $film2 = new  Film($this->getReference('categorie1'));
        $film3 = new  Film($this->getReference('categorie2'));
        $film4 = new  Film($this->getReference('categorie1'));

        $image1 =new Image();
        $image1->setPath('avatar.jpg');
        $image1->setAlt('Avatar');

        $image2 =new Image();
        $image2->setPath('johnwick2.jpg');
        $image2->setAlt('John Wick 2');

        $image3 =new Image();
        $image3->setPath('jumanji.jpg');
        $image3->setAlt('Jumanji');

        $image4 =new Image();
        $image4->setPath('expendables3.jpg');
        $image4->setAlt('Expendable 3');

        $film1->setTitle("Avatar");
        $film1->setDescription("Malgré sa paralysie, Jake Sully, un ancien marine immobilisé
             dans un fauteuil roulant, est resté un combattant au plus profond de son être.
              Il est recruté pour se rendre à des années-lumière de la Terre, sur Pandora,
               où de puissants groupes industriels exploitent un minerai rarissime destiné
                à résoudre la crise énergétique sur Terre. Parce que l'atmosphère de 
                Pandora est toxique pour les humains, ceux-ci ont créé le Programme Avatar,
                 qui permet à des \" pilotes \" humains de lier leur esprit à un avatar,
                  un corps biologique commandé à distance, capable de survivre dans cette
                   atmosphère létale. Ces avatars sont des hybrides créés génétiquement 
                   en croisant l'ADN humain avec celui des Na'vi, les autochtones de Pandora.
                Sous sa forme d'avatar, Jake peut de nouveau marcher. On lui confie une mission
                 d'infiltrationauprès des Na'vi, devenus un obstacle trop conséquent à l'exploitation
                  du précieux minerai. Mais tout va changer lorsque Neytiri, une très belle Na'vi, 
                  sauve la vie de Jake...");
        $film1->setReleaseDate(new \DateTime("2009-12-16 00:00:00"));
        $film1->setProducer("James Cameron");
        $film1->setDirector("James Cameron");
        $film1->setAgeRestriction("0");
        $film1->setImage($image1);


        $film2->setTitle("John Wick 2 ");
        $film2->setDescription("John Wick est forcé de sortir de sa retraite volontaire par un de ses ex-associés qui cherche à prendre le contrôle d’une mystérieuse confrérie de tueurs internationaux. Parce qu’il est lié à cet homme par un serment, John se rend à Rome, où il va devoir affronter certains des tueurs les plus dangereux du monde.");
        $film2->setReleaseDate(new \DateTime("2017-02-22 00:00:00"));
        $film2->setProducer("Chad Stahelski");
        $film2->setDirector("Chad Stahelski");
        $film2->setAgeRestriction("12");
        $film2->setImage($image2);

        $film3->setTitle("Jumanji");
        $film3->setDescription("Le destin de quatre lycéens en retenue bascule lorsqu’ils sont aspirés dans le monde de Jumanji. Après avoir découvert une vieille console contenant un jeu vidéo dont ils n’avaient jamais entendu parler, les quatre jeunes se retrouvent mystérieusement propulsés au cœur de la jungle de Jumanji, dans le corps de leurs avatars. Ils vont rapidement découvrir que l’on ne joue pas à Jumanji, c’est le jeu qui joue avec vous… Pour revenir dans le monde réel, il va leur falloir affronter les pires dangers et triompher de l’ultime aventure. Sinon, ils resteront à jamais prisonniers de Jumanji…");
        $film3->setReleaseDate(new \DateTime("2017-12-20 00:00:00"));
        $film3->setProducer("Jake Kasdan");
        $film3->setDirector("Jake Kasdan");
        $film3->setAgeRestriction("0");
        $film3->setImage($image3);

        $film4->setTitle("Expandable 3");
        $film4->setDescription("Barney, Christmas et le reste de l’équipe affrontent Conrad Stonebanks, qui fut autrefois le fondateur des Expendables avec Barney. Stonebanks devint par la suite un redoutable trafiquant d’armes, que Barney fut obligé d’abattre… Du moins, c’est ce qu’il croyait. 
Ayant échappé à la mort, Stonebanks a maintenant pour seul objectif d’éliminer l’équipe des Expendables. Mais Barney a d’autres plans... Il décide d’apporter du sang neuf à son unité spéciale et d’engager de nouveaux équipiers plus jeunes, plus vifs et plus calés en nouvelles technologies. Cette mission se révèle rapidement un choc des cultures et des générations, entre adeptes de la vieille école et experts high-tech. 
Les Expendables vont livrer leur bataille la plus explosive et la plus personnelle…");
        $film4->setReleaseDate(new \DateTime("2014-08-20 00:00:00"));
        $film4->setProducer(" Patrick Hughes");
        $film4->setDirector(" Patrick Hughes");
        $film4->setAgeRestriction("16");
        $film4->setImage($image4);


        $manager->persist($film1);
        $manager->persist($film2);
        $manager->persist($film3);
        $manager->persist($film4);
        $manager->flush();

    }
}