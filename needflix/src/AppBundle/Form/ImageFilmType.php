<?php
/**
 * Created by PhpStorm.
 * User: Arnaud
 * Date: 28/02/2018
 * Time: 17:20
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageFilmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('file', FileType::class, ['label' => 'Image à insérer'])
            ->add('alt', TextType::class, ['label' => 'Entrez le alt de l\'image']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Image'
        ));
    }
}