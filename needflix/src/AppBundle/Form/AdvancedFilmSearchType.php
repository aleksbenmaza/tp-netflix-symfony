<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 28/02/2018
 * Time: 11:14
 */

namespace AppBundle\Form;


use AppBundle\Entity\Category;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AdvancedFilmSearchType extends FilmSearchType
{

    /**
     * @param FormBuilderInterface $builder
     * @param Category[] $categories
     */
    public function buildForm(FormBuilderInterface $builder, array $categories): void
    {
        parent::buildForm($builder, []);

        $builder->add('producer', TextType::class, ['attr' => ['placeholder' => 'entrez le nom d\'un producteur']])
                ->add('director', TextType::class, ['attr' => ['placeholder' => 'entrez le nom d\'un réalisateur']])
                ->add('release_year', IntegerType::class, ['attr' => ['placeholder' => 'entrez l\'année de sortie']])
                ->add('category_ids', EntityType::class, ['attr' => ['class' => 'form-control'], ['class' => Category::class, 'choice_label' => 'name', 'multiple ' => true, 'expanded' => true, 'choices' => $categories]])
                ->add('restriction_age', IntegerType::class, ['attr' => ['placeholder' => 'entrez un âge de réstriction']])
                ->add('minimum_rating', NumberType::class, ['attr' => ['placeholder' => 'entrez une note moyenne minimum']]);
    }
}