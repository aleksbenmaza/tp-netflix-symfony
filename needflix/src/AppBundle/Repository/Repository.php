<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 23/02/2018
 * Time: 15:56
 */

namespace AppBundle\Repository;


use Doctrine\Common\Util\ClassUtils;

trait Repository {

    private function saveAll(array $entities): void {
        foreach ($entities as $entity)
            $this->getEntityManager()->persist($entity);
    }

    private function removeAll(array $entities): void {
        if(count(array_count_values($entities)) > 1)
            throw new \InvalidArgumentException;
        if(!$entities)
            return;

        $stm = "DELETE FROM " . ClassUtils::getRealClass($entities[0]) . " entity "
             . "WHERE entity IN :entities";

        $this->getEntityManager()
             ->createQuery($stm)
             ->execute([
                 "entities" => $entities
             ]);
    }
}