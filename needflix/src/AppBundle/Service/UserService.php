<?php

namespace AppBundle\Service;

use AppBundle\Entity\Film;
use AppBundle\Entity\User;
use AppBundle\Input\Registration;
use AppBundle\Repository\FilmRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 26/02/2018
 * Time: 10:00
 */
class UserService
{

    /**
     * @var UserRepository $user_repository
     */
    private $user_repository;
    /**
     * @var FilmRepository $film_repository
     */
    private $film_repository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $password_encoder;

    /**
     * UserService constructor.
     * @param EntityManagerInterface $entity_manager
     * @param UserPasswordEncoderInterface $password_encoder
     */
    public function __construct(
        EntityManagerInterface       $entity_manager,
        UserPasswordEncoderInterface $password_encoder
    ) {
        $this->user_repository  = $entity_manager->getRepository(User::class);
        $this->film_repository  = $entity_manager->getRepository(Film::class);
        $this->password_encoder = $password_encoder;
    }

    /**
     * @return User[]
     */
    public function getAllUsers(): array
    {
        return $this->user_repository->findAll();
    }

    /**
     * @param int $id
     * @return User|null
     */
    public function getUserById(int $id): ? User
    {
        return $this->user_repository->find($id);
    }

    /**
     * @param Registration $registration
     * @return User
     */
    public function createUser(Registration $registration): ? User
    {

        $user = new User;
        $user->setFirstName($registration->getFirstName());
        $user->setLastName($registration->getLastName());
        $user->setEmailAddress($registration->getEmailAddress());
        $user->setHash(
            $this->password_encoder->encodePassword(
                $user,
                $registration->getPassword()
            )
        );
        $user->setBirthDate($registration->getBirthDate());
        $user->setRoleIndex(User::INDEX_ROLE_USER);

        $this->user_repository->save($user);

        return $user;
    }


}