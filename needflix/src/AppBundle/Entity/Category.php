<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="categories")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Film", mappedBy="category", orphanRemoval=true, cascade={"ALL"})
     */
    private $films;

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->films = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return void
     */
    public function setName(? string $name): void
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ? string
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Category
     */
    public function setCode(? string $code): void
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode(): ? string
    {
        return $this->code;
    }

    /**
     * @param Film $film
     * @return bool
     */
    public function addFilm(Film $film): bool
    {
        return $this->films->add($film);
    }

    /**
     * @return ArrayCollection
     */
    public function getFilms(): ArrayCollection
    {
        return $this->films;
    }

    /**
     * Remove film.
     *
     * @param \AppBundle\Entity\Film $film
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFilm(\AppBundle\Entity\Film $film)
    {
        return $this->films->removeElement($film);
    }
}
