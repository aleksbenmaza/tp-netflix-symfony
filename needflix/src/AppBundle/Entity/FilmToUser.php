<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 23/02/2018
 * Time: 14:16
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * FilmToUser
 *
 * @ORM\Table("films_to_users")
 * @ORM\Entity()
 */
class FilmToUser
{

    /**
     * @var Film
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Film", inversedBy="filmsToUsers", fetch="EAGER", cascade={"ALL"})
     * @ORM\JoinColumn(name="film_id", referencedColumnName="id", nullable=false)
     */
    private $film;

    /**
     * @var User
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="User", inversedBy="filmsToUsers", fetch="EAGER", cascade={"ALL"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $rating;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $favourite;

    /**
     * FilmToUser constructor.
     * @param Film $film
     * @param User $user
     */
    public function __construct(Film $film, User $user) {
        $this->film = $film;
        $this->user = $user;
    }

    /**
     * @return Film
     */
    public function getFilm(): Film
    {
        return $this->film;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return float|null
     */
    public function getRating(): ? float
    {
        return $this->rating;
    }

    /**
     * @param float|null $rating
     */
    public function setRating(? float $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @return bool|null
     */
    public function isFavourite(): ? bool
    {
        return $this->favourite;
    }

    /**
     * @param bool|null $favourite
     */
    public function setFavourite(? bool $favourite): void
    {
        $this->favourite = $favourite;
    }

    /**
     * Get favourite.
     *
     * @return bool
     */
    public function getFavourite()
    {
        return $this->favourite;
    }

    /**
     * Set film.
     *
     * @param \AppBundle\Entity\Film $film
     *
     * @return FilmToUser
     */
    public function setFilm(\AppBundle\Entity\Film $film)
    {
        $this->film = $film;

        return $this;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return FilmToUser
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }
}
