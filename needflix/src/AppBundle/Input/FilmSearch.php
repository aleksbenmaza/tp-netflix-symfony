<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 27/02/2018
 * Time: 10:31
 */

namespace AppBundle\Input;


class FilmSearch {

    /**
     * @var null|string
     */
    private $name_like;

    /**
     * @return null|string
     */
    public function getNameLike(): ? string {
        return $this->name_like;
    }

    /**
     * @param null|string $name_like
     */
    public function setNameLike(? string $name_like): void {
        $this->name_like = $name_like;
    }
}