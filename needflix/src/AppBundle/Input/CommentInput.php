<?php

namespace AppBundle\Input;

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 26/02/2018
 * Time: 10:05
 */
class CommentInput {

    /**
     * @var int film_id
     */
    private $film_id;

    /**
     * @var int user_id
     */
    private $user_id;

    /**
     * @var string content
     */
    private $content;

    /**
     * @return int|null
     */
    public function getFilmId(): ? int
    {
        return $this->film_id;
    }

    /**
     * @param int|null $film_id
     */
    public function setFilmId(? int $film_id): void
    {
        $this->film_id = $film_id;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ? int
    {
        return $this->user_id;
    }

    /**
     * @param int|null $user_id
     */
    public function setUserId(? int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string|null
     */
    public function getContent(): ? string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     */
    public function setContent(? string $content): void
    {
        $this->content = $content;
    }
}