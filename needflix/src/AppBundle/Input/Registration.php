<?php

namespace AppBundle\Input;

use AppBundle\Validator\Constraints\UniqueEntityProperty;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 26/02/2018
 * Time: 10:03
 */
class Registration {

    /**
     * @var string
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $first_name;

    /**
     * @var string
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $last_name;

    /**
     * @var string
     * @Assert\NotNull()
     * @Assert\Email()
     * @UniqueEntityProperty(propertyName="emailAddress", entityClass="AppBundle\Entity\User")
     */
    private $email_address;

    /**
     * @var string
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @var \DateTime
     * @Assert\NotNull()
     */
    private $birth_date;

    /**
     * @return string
     */
    public function getFirstName(): ? string
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName(? string $first_name): void
    {
        $this->first_name = $first_name;
    }

    /**
     * @return string
     */
    public function getLastName(): ? string
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName(? string $last_name): void
    {
        $this->last_name = $last_name;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): ? string
    {
        return $this->email_address;
    }

    /**
     * @param string $email_address
     */
    public function setEmailAddress(? string $email_address): void
    {
        $this->email_address = $email_address;
    }

    /**
     * @return string
     */
    public function getPassword(): ? string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(? string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate(): ? \DateTime
    {
        return $this->birth_date;
    }

    /**
     * @param \DateTime $birth_date
     */
    public function setBirthDate(? \DateTime $birth_date): void
    {
        $this->birth_date = $birth_date;
    }
}